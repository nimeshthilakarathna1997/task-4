package unitTest;

import java.lang.reflect.Method;
import java.util.List;
import java.util.ArrayList;

public class MyUnitTestFramework {
	private List<String> logData;

	public MyUnitTestFramework() {
		logData = new ArrayList<>();
	}

	public void testMethods(Object object) {
		String methodName;
		Class<?> cls = object.getClass();
		
		Method[] methods = cls.getDeclaredMethods();
		for (Method method : methods) {
			methodName = method.getName();
			if (methodName.startsWith("check")) {
				try {
					method.setAccessible(true);
					method.invoke(object);
					logTestSuccess(methodName);
				} catch (Exception e) {
					logTestFailure(methodName, e.getCause().getMessage());
				}
			}
		}
		printLogData();
	}

	private void logTestSuccess(String methodName) {
		logData.add("SUCCESS: " + methodName);
	}

	private void logTestFailure(String methodName, String error) {
		logData.add("FAILURE: " + methodName + " - " + error);
	}

	private void printLogData() {
		System.out.println("===== LOG DATA =====");
		for (String result : logData) {
			System.out.println(result);
		}
	}

	public static void assertEquals(Object expected, Object actual) {
		if (!expected.equals(actual)) {
			throw new AssertionError("Expected: " + expected + "\tActual: " + actual);
		}
	}
	
	public static void assertNotEquals(Object compareValue, Object actual) {
		if (compareValue.equals(actual)) {
			throw new AssertionError("Value should not be equal to " + compareValue);
		}
	}
	
	public static void assertNull(Object object) {
		if(object != null) {
			throw new AssertionError("Expected: Null\tActual: Not Null");
		}
	}
	
	public static void assertNotNull(Object object) {
		if(object == null) {
			throw new AssertionError("Expected: Not Null\tActual: Null");
		}
	}
	
	public static void assertTrue(boolean val) {
		if(!val) {
			throw new AssertionError("Expected: True\tActual: False");
		}
	}
	
	public static void assertFalse(boolean val) {
		if(val) {
			throw new AssertionError("Expected: False\tActual: True");
		}
	}
}
