package unitTest;

public class Calculator {
	double answer;
	public double addition(double number1, double number2) {
		answer = number1+number2;
		return answer;
	}
	
	public double subtraction(double number1, double number2) {
		answer = number1-number2;
		return answer;
	}
	
	public double multiplication(double number1, double number2) {
		answer = number1*number2;
		return answer;
	}
	
	public double division(double number1, double number2) throws ArithmeticException{
		answer = number1/number2;
		return answer;
	}
	
	public String toString() {
		return Double.toString(answer);
	}
}
