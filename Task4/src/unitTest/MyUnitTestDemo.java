package unitTest;

public class MyUnitTestDemo {

	public static void main(String[] args) {
		MyUnitTestFramework myUnitTestFramework = new MyUnitTestFramework();
		CalculatorTest calculatorTest = new CalculatorTest();
		
		myUnitTestFramework.testMethods(calculatorTest);
	}

}
