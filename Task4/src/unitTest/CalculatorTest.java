package unitTest;

public class CalculatorTest {
	Calculator calculator;

	public CalculatorTest() {
		this.calculator = new Calculator();
	}

	public void checkTwoPlusTwoEqualsFour() {
		MyUnitTestFramework.assertEquals(4.0, calculator.addition(2, 2));
	}

	public void checkThreePlusFiveEqualsEight() {
		MyUnitTestFramework.assertTrue(calculator.addition(3, 5) == 8);
	}

	public void checkTenMinusFourEqualsSix() {
		MyUnitTestFramework.assertEquals(6.0, calculator.subtraction(10, 4));
	}

	public void checkThreeMultipliedByFiveEqualsFifteen() {
		MyUnitTestFramework.assertEquals(15.0, calculator.multiplication(3, 5));
	}
	
	public void checkTenDividedByFiveEqualsTwo() {
		MyUnitTestFramework.assertEquals(2.0, calculator.division(10, 5));
	}
	
	public void checkAnswer() {
		MyUnitTestFramework.assertNotNull(calculator.toString());
	}
	

}
