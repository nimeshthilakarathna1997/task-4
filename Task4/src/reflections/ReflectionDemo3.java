package reflections;

import java.lang.reflect.Field;

public class ReflectionDemo3 {

	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
		SimpleCalculator simpleCalculator = new SimpleCalculator(5, 2);

		Class<?> cls = simpleCalculator.getClass();

		Field[] fields = cls.getDeclaredFields();
		
		System.out.println(simpleCalculator.getNumber2());

		for (Field field : fields) {
			if (field.getName().equals("number2")) {
				field.setAccessible(true);
				field.set(simpleCalculator,7);
			}
		}
		System.out.println(simpleCalculator.getNumber2());

	}

}
