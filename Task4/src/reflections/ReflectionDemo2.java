package reflections;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionDemo2 {

	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		SimpleCalculator simpleCalculator = new SimpleCalculator(5, 2);

		Class<?> cls = simpleCalculator.getClass();
		
		Method[] methods = cls.getDeclaredMethods();
		
		simpleCalculator.addition();
		System.out.println(simpleCalculator.toString());
		
		for(Method method: methods) {
			if(method.getName().equals("subtraction")) {
				method.setAccessible(true);
				method.invoke(simpleCalculator);
			}
		}
		
		System.out.println(simpleCalculator.toString());

	}

}
