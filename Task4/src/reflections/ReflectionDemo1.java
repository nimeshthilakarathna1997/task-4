package reflections;

import java.lang.reflect.Field;

public class ReflectionDemo1 {
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
		SimpleCalculator simpleCalculator = new SimpleCalculator(5, 2);
		
		Class<?> cls = simpleCalculator.getClass();
		
		Field[] fields = cls.getDeclaredFields();
		
		for(Field field: fields) {
			if(field.getName().equals("number2")){
				field.setAccessible(true);
				System.out.println(field.getDouble(simpleCalculator));
			}
		}
	}
}
