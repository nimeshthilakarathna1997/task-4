package reflections;

public class SimpleCalculator {
	public double number1;
	private double number2;
	
	private double answer = 0;
	
	public SimpleCalculator(double number1, double number2) {
		this.number1 = number1;
		this.number2 = number2;
	}
	
	public void addition() {
		answer = number1+number2;
	}
	
	private void subtraction() {
		answer = number1-number2;
	}
	
	public void setNumber1(double number1) {
		this.number1 = number1;
	}
	
	private double getNumber1() {
		return number1;
	}
	
	public void setNumber2(double number2) {
		this.number2 = number2;
	}
	
	public double getNumber2() {
		return number2;
	}
	
	private double getAnswer() {
		return answer;
	}
	
	public String toString() {
		return String.format("{Answer :%.2f", answer);
	}
	
}
